<?php
$data = file_get_contents(__DIR__ . '/lesson2.1PhoneNumbers.json');
$phoneNumbers= json_decode($data, true);
?>


<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title>Список номеров</title>
	</head>
	<body>
	<table border = "1">
		<tr>
			<td>Имя</td>
			<td>Фамилия</td>
			<td>Номер</td>
		</tr>
	<?php foreach ($phoneNumbers as $number) { ?>
		<tr>
			<td><?php echo $number ['firstName']; ?></td>
			<td><?php echo $number ['lastName']; ?></td>
			<td><?php echo $number ['phoneNumber']; ?></td>
		</tr>
	<?php } ?>
	</table>	
	</body>
</html>